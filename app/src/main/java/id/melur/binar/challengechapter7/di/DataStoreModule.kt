package id.melur.binar.challengechapter7.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.melur.binar.challengechapter7.datastore.LoggedDataStoreManager
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataStoreModule {
    @Singleton
    @Provides
    fun provideUserPreference(
        @ApplicationContext appContext: Context
    ) = LoggedDataStoreManager(appContext)
}
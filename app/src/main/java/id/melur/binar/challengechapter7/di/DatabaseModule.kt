package id.melur.binar.challengechapter7.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.melur.binar.challengechapter7.database.UserDatabase
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Singleton
    @Provides
    fun providesUserDao(database: UserDatabase) = database.userDao()

}
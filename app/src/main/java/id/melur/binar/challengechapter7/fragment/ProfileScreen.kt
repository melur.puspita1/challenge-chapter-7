package id.melur.binar.challengechapter7.fragment

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter7.R
import id.melur.binar.challengechapter7.viewmodel.ViewModel
import id.melur.binar.challengechapter7.databinding.FragmentProfileScreenBinding
import id.melur.binar.challengechapter7.datastore.LoggedDataStoreManager
import id.melur.binar.challengechapter7.helper.UserRepo
import id.melur.binar.challengechapter7.helper.viewModelsFactory
import id.melur.binar.challengechapter7.service.TMDBApiService
import id.melur.binar.challengechapter7.service.TMDBClient
import id.melur.binar.challengechapter7.viewmodel.LoggedViewModel
import java.text.SimpleDateFormat
import java.util.*

class ProfileScreen : Fragment() {

    private var _binding: FragmentProfileScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences
    private var dataUsername: String? = ""

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val api: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, api) }

    private val pref: LoggedDataStoreManager by lazy { LoggedDataStoreManager(requireContext()) }
    private val loggedViewModel: LoggedViewModel by viewModelsFactory { LoggedViewModel(pref) }

    private var userId: Int = 0
    private var email: String = ""
    private var password: String = ""
    private var image: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentProfileScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPref = context.getSharedPreferences("username", Context.MODE_PRIVATE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        getData()
        val username = sharedPref.getString("username", "")
        binding.etUsernamee.setText("$username")

        viewModel.coba("$username")

        profileField()
        logoutButtonOnPressed()
        updateButtonOnPressed()
        imageButtonOnPressed()
    }

    private fun getData() {
        dataUsername = sharedPref.getString("username", "")
    }

    private fun updateButtonOnPressed(){
        binding.btnUpdate.setOnClickListener {

            val username = binding.etUsernamee.text.toString()
            val name = binding.etNama.text.toString()
            val date = binding.etTanggal.text.toString()
            val address = binding.etAlamat.text.toString()

            viewModel.updateUser(username, name, date, address)
            Toast.makeText(requireContext(), "Data telah diupdate", Toast.LENGTH_SHORT).show()
        }
    }

    private fun profileField(){
        Handler(Looper.getMainLooper()).postDelayed({
            val name = viewModel.name.value.toString()
            val dateOfBirth = viewModel.dateOfBirth.value.toString()
            val address = viewModel.address.value.toString()

            binding.etNama.setText("$name")
            binding.etTanggal.setText("$dateOfBirth")
            binding.etAlamat.setText("$address")

            binding.pbProfile.isVisible = false
        },200)
    }

    private fun logoutButtonOnPressed(){
        binding.btnLogout.setOnClickListener {
            val editor = sharedPref.edit()
            editor.clear()
            editor.apply()
            dataUsername = sharedPref.getString("username", "")
            Toast.makeText(requireContext(), "Logout berhasil", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_profileScreen_to_loginScreen)
        }
    }

    private fun observeData() {
        viewModel.user2.observe(viewLifecycleOwner) {
            if (it != null) {
                binding.apply {
                    etUsernamee.setText(it.username)
                    etNama.setText(it.name)
                    etTanggal.setText(it.dateOfBirth)
                    etAlamat.setText(it.address)
                }
                userId = it.userId!!.toInt()
                email = it.email
                password = it.password

            }
        }
        viewModel.update.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), "Update Berhasil!", Toast.LENGTH_SHORT)
                .show()
        }

        viewModel.failUpdate.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), "Update Gagal!", Toast.LENGTH_SHORT)
                .show()
        }

        loggedViewModel.getEmail().observe(viewLifecycleOwner) {
            // harusnya gapake 2?
            viewModel.getUser2(it)
        }
    }

    private fun imageButtonOnPressed() {
        binding.profileImage.setOnClickListener {
            checkingPermissions()
        }
    }

    private fun checkingPermissions() {
        if (isGranted(
                requireActivity(),
                Manifest.permission.CAMERA,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                1,
            )
        ) {
//            chooseImageDialog()
            openGallery()
        }
    }

    private fun isGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        request: Int,
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(activity, permissions, request)
            }
            false
        } else {
            true
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, Please allow permissions from App Settings.")
            .setPositiveButton(
                "App Settings"
            ) { _, _ ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri = Uri.fromParts("package", requireActivity().packageName, null)
                intent.data = uri
                startActivity(intent)
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            .show()
    }


    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
            val uriString = result.toString()
            binding.profileImage.setImageURI(Uri.parse(uriString))
            image = uriString
        }

    private fun openGallery() {
        requireActivity().intent.type = "image/*"
        galleryResult.launch("image/*")
        binding.addImage.isVisible = false
    }
}
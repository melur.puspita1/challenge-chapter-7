package id.melur.binar.challengechapter7.helper

import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.text.SimpleDateFormat
import java.util.*

inline fun <reified T : ViewModel> ComponentActivity.viewModelsFactory(crossinline viewModelInitialization: () -> T): Lazy<T> {
    return viewModels {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return viewModelInitialization.invoke() as T
            }
        }
    }
}

inline fun <reified T : ViewModel> Fragment.viewModelsFactory(crossinline viewModelInitialization: () -> T): Lazy<T> {
    return viewModels {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return viewModelInitialization.invoke() as T
            }
        }
    }
}

fun String.toDate(): String? {
    if (this.isEmpty()) {
        return "-"
    }
    // pattern tanggal dari api
    val inputPattern = "yyyy-MM-dd"
    // pattern yang kita inginkan
    val outputPattern = "MMMM dd, (yyyy)"

    val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
    val outputFormat = SimpleDateFormat(outputPattern, Locale("in"))

    // Parsing tanggal dari api, this itu adalah String yang di pake buat manggil toDate()
    // parsing adalah ubah tipe data string menjadi tipe data Date
    val inputDate = inputFormat.parse(this)

    // .format adalah ubah tipe data Date menjadi tipe data String
    return inputDate?.let {
        outputFormat.format(it)
    }
}

fun Int.toHourMinutes(): String {
    val hour = this / 60
    val minutes = this % 60

    return "$hour Jam $minutes Menit"
}

fun Double.toPercentage(): String {
    val voteAverage = (this * 10).toInt()
    return "$voteAverage%"
}

fun String.toYear(): String {
    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val outputFormat = SimpleDateFormat("(yyyy)", Locale.getDefault())

    val inputDate = inputFormat.parse(this)
    inputDate.let {
        return outputFormat.format(it)
    }
}
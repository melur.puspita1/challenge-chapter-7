package id.melur.binar.challengechapter7.model


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
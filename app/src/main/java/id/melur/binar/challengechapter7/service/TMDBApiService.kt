package id.melur.binar.challengechapter7.service

import id.melur.binar.challengechapter7.model.MoviePopular
import id.melur.binar.challengechapter7.model.MoviePopularItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBApiService {

    @GET("movie/popular")
    fun getMoviePopular(@Query("api_key") key: String) : Call<MoviePopular>

    @GET("movie/{movie_id}")
    fun getDetailMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String
    ): Call<MoviePopularItem>

    @GET("movie/popular")
    suspend fun getAllMovie(@Query("api_key") key: String): MoviePopular

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(
        @Path("movie_id") movieId: Int,
        @Query("api_key") key: String
    ): MoviePopularItem

}
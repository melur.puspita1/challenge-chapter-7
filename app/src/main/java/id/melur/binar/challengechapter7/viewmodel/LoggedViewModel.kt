package id.melur.binar.challengechapter7.viewmodel

import androidx.lifecycle.*
import androidx.lifecycle.ViewModel
import id.melur.binar.challengechapter7.datastore.LoggedDataStoreManager
import kotlinx.coroutines.launch

class LoggedViewModel(private val pref: LoggedDataStoreManager) : ViewModel() {
    val email = MutableLiveData<String>()

    fun saveData(id: Int, username: String, email: String, password: String) {
        viewModelScope.launch {
            pref.saveDataLoged(id, username, email, password)
        }
    }

    fun checkLogin(): LiveData<Boolean> {
        return pref.getLogin().asLiveData()
    }

    fun getUserId(): LiveData<Int> {
        return pref.getUserId().asLiveData()
    }

    fun getPassword(): LiveData<String> {
        return pref.getPassword().asLiveData()
    }

    fun getEmail(): LiveData<String> {
        return pref.getEmail().asLiveData()
    }

    fun clearData() {
        viewModelScope.launch {
            pref.clearData()
        }
    }

}